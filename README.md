# nuav_path_planning

## Setup 

### Simulation 
To install gazebo simulator, follow the steps [here](https://docs.px4.io/main/en/simulation/gazebo.html)
Please use a windows computer with Windows 11 or else it will not work :(. 
```
cd /path/to/PX4-Autopilot
make px4_sitl gazebo
// --will start the gazebo terminal-- 
// --wait until the prompt "pxh>"-- 
pxh> param set NAV_RCL_ACT 0 
pxh> param set COM_RCL_EXCEPT 4	
```
### MAVSDK 
Download MAVSDK Python libraries [here](https://github.com/mavlink/MAVSDK-Python) 
This is how we will control the drone. 
## Clone your repo

```
cd existing_repo
git clone https://gitlab.com/aeronu/nuav/nuav_path_planning.git
```
